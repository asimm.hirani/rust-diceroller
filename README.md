# Rust DiceRoller Web Service
This is a web service built using Rust to simulate dice rolls with various expressions. The service listens on 127.0.0.1:3000.

## Demo
![Image showing webpage of diceroller](img/demo.png "Demo Image")

## Features:
- API endpoint to roll dice based on given expressions.
- API endpoint to check version of the service.
- An HTML interface to interact with the dice roller.

## Endpoints:
1. /rollapi/v1/roll (POST): This endpoint accepts a JSON payload with iterations (number of times to evaluate the roll expression) and rollstring (the roll expression itself). The response will be a list of results for each iteration.

### Example Payload: 
```json
 {"iterations": 3, "rollstring": "3d9+2d9 <= 12"} 
 ```

2. /rollapi/v1/version (GET): Returns the version of the service.

3. /diceroller (GET): This endpoint serves an HTML interface for users to interact with the dice roller.

## Running the Service:
Ensure you have Rust and Cargo installed.

1. Clone the repository.
2. Navigate to the repository directory.
3. Run ```cargo run --release``` to start the service.
4. The service will start listening on 127.0.0.1:3000.

## Tests:
The project also contains tests to validate JSON parsing and different roll expressions. To run the tests:

```bash
cargo test
```
## Roll Expressions:
The service can evaluate complex dice expressions, such as:

- Basic rolls like "3d6"
- Modifiers like "3d6+2"
- Comparisons like "3d6 <= 12"
- Simulating attacks against enemies with known AC (no crit):
  - (2d20kh1 + 1d4 + 3 + 4 - 5 >= 14)*(2d6 + 3d8 + 3 + 10)
- Evaluating different strategies:
  - To Use GWM or Not?
    - (2d20kh1 + 1d4 + 3 + 4 - 5 >= 14)*(2d6 + 3d8 + 3 + 10) - (2d20kh1 + 1d4 + 3 + 4 >= 14)*(2d6 + 3d8 + 3)
- For more advanced use-cases and expressions, please refer to the expressions module.

## Dependencies:
axum for the web framework.

rayon for parallelism.

rclite for reference counting.

num_cpus to get the number of CPUs.

## Limitations:
The maximum iterations for a single query of the dice roll are currently set to 500,000.
