use rand::prelude::*;

pub struct DiceExpressionParser;

impl DiceExpressionParser {
    pub fn parse_rpn(expression_str: &str) -> Result<RPNExpression, ParseError> {
        let expression_str: String = expression_str
            .chars()
            .filter(|c| !c.is_whitespace())
            .map(|c| c.to_ascii_lowercase())
            .collect();
        let mut char_iterator = expression_str.chars().peekable();
        let mut output_vec = Vec::new();
        let mut operator_vec = Vec::new();
        while let Some(character) = char_iterator.next() {
            // Numeric literal
            if character.is_ascii_digit() {
                let mut buffer = String::new();
                buffer.push(character);
                while let Some(peek) = char_iterator.peek() {
                    if peek.is_numeric() {
                        buffer.push(*peek);
                        char_iterator.next();
                    } else {
                        break;
                    }
                }
                let token = Token::Operand(OperandToken::Numeric(match buffer.parse() {
                    Ok(num) => num,
                    // Logic: IntParseError will only be found if trailing whitespace or nonnumeric chars
                    // Therefore we should not worry about that parse error here.
                    Err(_) => return Err(ParseError::ErrorParsingInt(buffer)), 
                }));
                output_vec.push(token);
            } else {
                let token = match character {
                    '+' => Ok(Token::Operator(OperatorToken::Plus)),
                    '-' => Ok(Token::Operator(OperatorToken::Minus)),
                    '*' => Ok(Token::Operator(OperatorToken::Multiply)),
                    '/' => Ok(Token::Operator(OperatorToken::Divide)),
                    '(' | '[' | '{' => Ok(Token::Grouping(GroupingToken::Open(character))),
                    ')' | ']' | '}' => Ok(Token::Grouping(GroupingToken::Close(character))),
                    'd' => Ok(Token::Operator(OperatorToken::DiceRoll)),
                    'k' => {
                        let next = char_iterator.peek();
                        let token = match next {
                            Some(&'h') => Ok(Token::Operator(OperatorToken::KeepHighest)),
                            Some(&'l') => Ok(Token::Operator(OperatorToken::KeepLowest)),
                            Some(next) => Err(ParseError::UnexpectedToken(*next)),
                            None => Err(ParseError::UnexpectedEnd),
                        }?;
                        char_iterator.next();
                        Ok(token)
                    }
                    '<' => {
                        if let Some(ch) = char_iterator.peek() {
                            if *ch == '=' {
                                char_iterator.next();
                                Ok(Token::Operator(OperatorToken::LessThanEqualTo))
                            } else {
                                Ok(Token::Operator(OperatorToken::LessThan))
                            }
                        } else {
                            Ok(Token::Operator(OperatorToken::LessThan))
                        }
                    }
                    '>' => {
                        if let Some(ch) = char_iterator.peek() {
                            if *ch == '=' {
                                char_iterator.next();
                                Ok(Token::Operator(OperatorToken::GreaterThanEqualTo))
                            } else {
                                Ok(Token::Operator(OperatorToken::GreaterThan))
                            }
                        } else {
                            Ok(Token::Operator(OperatorToken::GreaterThan))
                        }
                    }
                    '=' => {
                        if let Some(ch) = char_iterator.peek() {
                            if *ch == '=' {
                                char_iterator.next();
                                Ok(Token::Operator(OperatorToken::StrictlyEqual))
                            } else {
                                Err(ParseError::UnexpectedToken(*ch))
                            }
                        } else {
                            Err(ParseError::UnexpectedEnd)
                        }
                    }
                    _ => Err(ParseError::UnexpectedToken(character)),
                }?;
                match &token {
                    Token::Operand(_) => output_vec.push(token),
                    Token::Grouping(grouping) => {
                        if let GroupingToken::Open(_) = grouping {
                            operator_vec.push(token);
                        } else if let GroupingToken::Close(close_group) = grouping {
                            loop {
                                let Some(op) = operator_vec.pop() else {
                                    return Err(ParseError::InvalidGrouping);
                                };
                                if let Token::Grouping(GroupingToken::Open(open_group)) = op {
                                    if matching_group(open_group, *close_group) {
                                        break;
                                    } else {
                                        return Err(ParseError::InvalidGrouping);
                                    }
                                } else {
                                    output_vec.push(op);
                                }
                            }
                        }
                    }
                    Token::Operator(current_op) => {
                        while let Some(Token::Operator(top_op)) = operator_vec.last() {
                            if top_op.precedence() > current_op.precedence() {
                                let op = operator_vec.pop().unwrap();
                                output_vec.push(op);
                            } else {
                                break;
                            }
                        }
                        operator_vec.push(token)
                    }
                }
            }
        }

        while let Some(token) = operator_vec.pop() {
            if let Token::Grouping(_) = token {
                return Err(ParseError::InvalidGrouping);
            }
            output_vec.push(token);
        }

        Ok(RPNExpression {
            token_expression: output_vec,
        })
    }
}

#[derive(Debug, serde::Serialize)]
pub enum ParseError {
    UnexpectedToken(char),
    InvalidGrouping,
    UnexpectedEnd,
    ErrorParsingInt(String)
}

pub trait Expression {
    fn eval(&self) -> Result<i64, EvaluationError>;
}

#[derive(Debug, Clone)]
enum Token {
    Operator(OperatorToken),
    Operand(OperandToken),
    Grouping(GroupingToken),
}

#[derive(Debug, Clone)]
pub enum OperandToken {
    Numeric(i64),
    DiceSet(DiceSet),
    Boolean(bool),
}

impl OperandToken {
    fn flatten(&self) -> Result<i64, EvaluationError> {
        Ok(match self {
            OperandToken::Numeric(n) => *n,
            OperandToken::DiceSet(d) => {
                d.0.iter()
                    .try_fold(0i64, |accumulator, elem| accumulator.checked_add(*elem))
                    .ok_or(EvaluationError::IntegerOverflow)?
            }
            OperandToken::Boolean(b) if *b => 1,
            OperandToken::Boolean(b) if !(*b) => 0,
            _ => unreachable!(),
        })
    }
}

#[derive(Debug, Clone)]
pub struct DiceSet(Vec<i64>);

#[derive(Debug, Clone, Copy)]
pub enum OperatorToken {
    Plus,
    Minus,
    Multiply,
    Divide,
    LessThan,
    GreaterThan,
    StrictlyEqual,
    LessThanEqualTo,
    GreaterThanEqualTo,
    DiceRoll,
    KeepHighest,
    KeepLowest,
}

impl OperatorToken {
    fn precedence(&self) -> i32 {
        match self {
            OperatorToken::Plus => 1,
            OperatorToken::Minus => 1,
            OperatorToken::Multiply => 2,
            OperatorToken::Divide => 2,
            OperatorToken::DiceRoll => 4,
            OperatorToken::KeepHighest => 3,
            OperatorToken::KeepLowest => 3,
            OperatorToken::LessThan => 0,
            OperatorToken::GreaterThan => 0,
            OperatorToken::StrictlyEqual => 0,
            OperatorToken::LessThanEqualTo => 0,
            OperatorToken::GreaterThanEqualTo => 0,
        }
    }
}

#[derive(Debug, Clone)]
enum GroupingToken {
    Open(char),
    Close(char),
}

#[derive(Debug, Clone)]
pub struct RPNExpression {
    token_expression: Vec<Token>,
}

fn matching_group(open: char, close: char) -> bool {
    matches!((open, close), ('(', ')') | ('[', ']') | ('{', '}'))
}

impl Expression for RPNExpression {
    fn eval(&self) -> Result<i64, EvaluationError> {
        // println!("RPN of Expression: {:?}", &self.token_expression);
        let mut rng = SmallRng::from_entropy();
        // let mut rng = ThreadRng::default();
        let mut operand_vec = Vec::new();
        for token in &self.token_expression {
            if let Token::Operand(operand) = token {
                operand_vec.push(operand.clone());
            } else if let Token::Operator(op) = token {
                let right = operand_vec
                    .pop()
                    .ok_or(EvaluationError::InsufficientArguments(*op))?;
                let left = operand_vec
                    .pop()
                    .ok_or(EvaluationError::InsufficientArguments(*op))?;
                let result = match op {
                    OperatorToken::Plus => add(&left, &right),
                    OperatorToken::Minus => sub(&left, &right),
                    OperatorToken::Multiply => mult(&left, &right),
                    OperatorToken::Divide => Err(EvaluationError::NotImplemented)?,
                    OperatorToken::LessThan => lt(&left, &right),
                    OperatorToken::GreaterThan => gt(&left, &right),
                    OperatorToken::StrictlyEqual => eq(&left, &right),
                    OperatorToken::LessThanEqualTo => lte(&left, &right),
                    OperatorToken::GreaterThanEqualTo => gte(&left, &right),
                    OperatorToken::DiceRoll => roll(&left, &right, &mut rng),
                    OperatorToken::KeepHighest => keep_highest(&left, &right),
                    OperatorToken::KeepLowest => keep_lowest(&left, &right),
                }?;
                operand_vec.push(result);
            } else {
                unreachable!();
            }
        }
        let Some(ans) = operand_vec.pop() else {
            return Err(EvaluationError::InternalError);
        };
        ans.flatten()
    }
}

fn add(left: &OperandToken, right: &OperandToken) -> Result<OperandToken, EvaluationError> {
    Ok(OperandToken::Numeric(
        left.flatten()?
            .checked_add(right.flatten()?)
            .ok_or(EvaluationError::IntegerOverflow)?,
    ))
}

fn sub(left: &OperandToken, right: &OperandToken) -> Result<OperandToken, EvaluationError> {
    Ok(OperandToken::Numeric(
        left.flatten()?
            .checked_sub(right.flatten()?)
            .ok_or(EvaluationError::IntegerUnderflow)?,
    ))
}

fn mult(left: &OperandToken, right: &OperandToken) -> Result<OperandToken, EvaluationError> {
    Ok(OperandToken::Numeric(
        left.flatten()?
            .checked_mul(right.flatten()?)
            .ok_or(EvaluationError::IntegerOverflow)?,
    ))
}

fn eq(left: &OperandToken, right: &OperandToken) -> Result<OperandToken, EvaluationError> {
    match (left, right) {
        (OperandToken::Boolean(l), OperandToken::Boolean(r)) => {
            return Ok(OperandToken::Boolean(l == r))
        }
        (OperandToken::Boolean(_), _) | (_, OperandToken::Boolean(_)) => {
            return Err(EvaluationError::InvalidOperands(
                left.clone(),
                right.clone(),
                "EqualTo",
            ))
        }
        _ => (),
    }

    Ok(OperandToken::Boolean(left.flatten()? == right.flatten()?))
}

fn lt(left: &OperandToken, right: &OperandToken) -> Result<OperandToken, EvaluationError> {
    match (&left, &right) {
        (OperandToken::Boolean(_), _) | (_, OperandToken::Boolean(_)) => {
            return Err(EvaluationError::InvalidOperands(
                left.clone(),
                right.clone(),
                "LessThan",
            ))
        }
        _ => (),
    }

    Ok(OperandToken::Boolean(left.flatten()? < right.flatten()?))
}

fn gt(left: &OperandToken, right: &OperandToken) -> Result<OperandToken, EvaluationError> {
    match (&left, &right) {
        (OperandToken::Boolean(_), _) | (_, OperandToken::Boolean(_)) => {
            return Err(EvaluationError::InvalidOperands(
                left.clone(),
                right.clone(),
                "GreaterThan",
            ))
        }
        _ => (),
    }

    Ok(OperandToken::Boolean(left.flatten()? > right.flatten()?))
}

fn lte(left: &OperandToken, right: &OperandToken) -> Result<OperandToken, EvaluationError> {
    match (&left, &right) {
        (OperandToken::Boolean(_), _) | (_, OperandToken::Boolean(_)) => {
            return Err(EvaluationError::InvalidOperands(
                left.clone(),
                right.clone(),
                "LessThan",
            ))
        }
        _ => (),
    }

    Ok(OperandToken::Boolean(left.flatten()? <= right.flatten()?))
}

fn gte(left: &OperandToken, right: &OperandToken) -> Result<OperandToken, EvaluationError> {
    match (&left, &right) {
        (OperandToken::Boolean(_), _) | (_, OperandToken::Boolean(_)) => {
            return Err(EvaluationError::InvalidOperands(
                left.clone(),
                right.clone(),
                "GreaterThan",
            ))
        }
        _ => (),
    }

    Ok(OperandToken::Boolean(left.flatten()? >= right.flatten()?))
}

fn roll<R: rand::Rng>(
    left: &OperandToken,
    right: &OperandToken,
    rng: &mut R,
) -> Result<OperandToken, EvaluationError> {
    let negative = (left.flatten()? < 0) != (right.flatten()? < 0);
    let prefix = if negative { -1 } else { 1 };

    let left = left.flatten()?.abs();
    let right = right.flatten()?.abs();

    if left >= 50000 {
        return Err(EvaluationError::TooManyDice)
    }

    if right == 0 {
        return Ok(OperandToken::DiceSet(DiceSet(vec![0;left as usize])));
    }
    let mut retval = DiceSet(
        (0..left)
            .map(|_| ((rng.next_u64() as i64).abs() % right + 1) * prefix)
            .collect(),
    );
    retval.0.sort_unstable();

    // println!("Dice Rolled: {}d{}: {:?}", left, right, retval);

    Ok(OperandToken::DiceSet(retval))
}

fn keep_highest(
    left: &OperandToken,
    right: &OperandToken,
) -> Result<OperandToken, EvaluationError> {
    let OperandToken::DiceSet(d) = left else {
        return Err(EvaluationError::InvalidOperands(left.clone(), right.clone(), "KeepHighest"));
    };
    let right_flat = right.flatten()?;
    match right_flat.cmp(&0) {
        std::cmp::Ordering::Less => Err(EvaluationError::CantKeepNegativeDice(
            left.clone(),
            right.clone(),
        )),
        std::cmp::Ordering::Equal => Ok(OperandToken::DiceSet(DiceSet(vec![0]))),
        std::cmp::Ordering::Greater => {
            let right_flat = right_flat as usize;

            if right_flat >= d.0.len() {
                Ok(OperandToken::DiceSet(d.clone()))
            } else {
                let dice_set = DiceSet(d.0[d.0.len() - right_flat..].to_vec());

                // println!("Keeping highest {} dice from {:?}. Result: {:?}",right_flat, d, dice_set);
                Ok(OperandToken::DiceSet(dice_set))
            }
        }
    }
}

fn keep_lowest(left: &OperandToken, right: &OperandToken) -> Result<OperandToken, EvaluationError> {
    let OperandToken::DiceSet(d) = left else {
        return Err(EvaluationError::InvalidOperands(left.clone(), right.clone(), "KeepHighest"));
    };
    let right_flat = right.flatten()?;
    match right_flat.cmp(&0) {
        std::cmp::Ordering::Less => Err(EvaluationError::CantKeepNegativeDice(
            left.clone(),
            right.clone(),
        )),
        std::cmp::Ordering::Equal => Ok(OperandToken::DiceSet(DiceSet(vec![0]))),
        std::cmp::Ordering::Greater => {
            let right_flat = right_flat as usize;

            if right_flat >= d.0.len() {
                Ok(OperandToken::DiceSet(d.clone()))
            } else {
                let dice_set = DiceSet(d.0[0..right_flat].to_vec());

                // println!("Keeping lowest {} dice from {:?}. Result: {:?}",right_flat, d, dice_set);
                Ok(OperandToken::DiceSet(dice_set))
            }
        }
    }
}

#[derive(Debug)]
pub enum EvaluationError {
    InsufficientArguments(OperatorToken),
    InvalidOperands(OperandToken, OperandToken, &'static str),
    InternalError,
    CantKeepNegativeDice(OperandToken, OperandToken),
    IntegerOverflow,
    IntegerUnderflow,
    NotImplemented,
    TooManyDice,
}
