#![no_main]

extern crate libdiceroller;

use libdiceroller::*;

use libfuzzer_sys::fuzz_target;

fuzz_target!(|data: &[u8]| {
    // fuzzed code goes here
    let string = match String::from_utf8(data.to_owned()) {
        Err(_) => return,
        Ok(val) => val
    };

    let _res = match libdiceroller::DiceExpressionParser::parse_rpn(&string) {
        Err(e) => return,
        Ok(expr) => expr.eval()
    };

});
