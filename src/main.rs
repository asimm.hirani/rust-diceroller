use std::{net::SocketAddr, thread, env::var};
use signal_hook::{consts::SIGTERM, iterator::Signals, low_level::exit};

use rayon::{prelude::*, *};
use rclite::Arc;

use tracing::{event, instrument, Level};

use libdiceroller::expressions;

use axum::{
    self,
    http::StatusCode,
    routing::{get, post},
    Json, Router, extract
};

use crate::expressions::*;

use tower_http::{
    compression::Compression,
    services::{ServeDir, ServeFile}
};

#[derive(Debug, serde::Deserialize)]
struct DiceRollRequest {
    iterations: usize,
    rollstring: String,
}

#[derive(Debug, serde::Serialize)]
struct DiceRollResponse {
    result: Vec<i64>,
}

#[tokio::main]
async fn main() {
    let uid = unsafe { libc::geteuid() };
    println!("Launched as user: {}.", uid);
    let debug = var("DEBUG").is_ok();
    let max_level = if debug {
        println!("Setting logging verbosity to DEBUG!");
        tracing::Level::DEBUG
    } else {
        println!("Setting logging verbosity to INFO!");
        tracing::Level::INFO
    };
    let max_iters = var("MAX_DICEROLLS").unwrap_or("50000".to_owned()).parse::<usize>().unwrap();

    let _guard = match var("APPLICATION_LOG_DIR") {
        Ok(logging_dir) => {
            println!("Log directory set: '{}'. Logging to file!", logging_dir);
            let file_appender = tracing_appender::rolling::daily(&logging_dir, "diceroller-rs.log");
            let (non_blocking, _guard) = tracing_appender::non_blocking(file_appender);
            tracing_subscriber::fmt()
                .with_max_level(max_level)
                .with_writer(non_blocking)
                .init();
            Some(_guard)
        },
        _ => {
            println!("Log directory not set! Logging to stdout!");
            tracing_subscriber::fmt()
                .with_max_level(max_level)
                .init();
            None
        }
    };

    ThreadPoolBuilder::new()
        .num_threads(num_cpus::get())
        .build_global()
        .unwrap();
    let app = Router::new()
        .nest_service("/rollapi/v1/roll", Compression::new(post(rtd).with_state(max_iters)))
        .route("/rollapi/v1/version", get(version))
        .nest_service("/", ServeFile::new("static/roll_portal.html"))
        .nest_service("/static", ServeDir::new("static"));
    
    let addr = SocketAddr::from(([0, 0, 0, 0], 3000));

    println!("Successfully initialized diceroller!");
    
    let mut signals = Signals::new([SIGTERM]).unwrap();
    thread::spawn(move || {
        for _sig in signals.forever() {
            event!(Level::WARN, "Received SIGTERM, shutting down!");
            exit(-1);
        }
    });
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

#[instrument]
async fn rtd(
    extract::State(max_iters): extract::State<usize>,
    Json(request): Json<DiceRollRequest>,
) -> Result<(StatusCode, Json<DiceRollResponse>), StatusCode> {
    event!(Level::INFO, "Received roll request!");
    if request.iterations > max_iters {
        event!(Level::WARN, "Rejecting request due to too many iterations");
        return Err(StatusCode::BAD_REQUEST);
    }
    match DiceExpressionParser::parse_rpn(&request.rollstring) {
        Ok(expr) => {
            let expr = Arc::new(expr);
            let input_vec = vec![expr; request.iterations];
            let result: Result<Vec<i64>, EvaluationError> =
                input_vec.into_par_iter().map(|iter| iter.eval()).collect();
            match result {
                Ok(result_vec) => Ok((
                    StatusCode::OK,
                    Json(DiceRollResponse { result: result_vec }),
                )),
                Err(e) => {
                    event!(Level::WARN, "Failed to evaluate expression, reason: {:?}", e);
                    Err(StatusCode::BAD_REQUEST)
                },
            }
        }
        Err(e) => {
            event!(Level::WARN, "Failed to parse expression, reason: {:?}", e);
            Err(StatusCode::BAD_REQUEST)
        },
    }
}

async fn version() -> (StatusCode, &'static str) {
    const VERSION: &str = env!("CARGO_PKG_VERSION");

    (StatusCode::OK, VERSION)
}



#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_json() {
        let json = r#"{"iterations": 3, "rollstring": "3d9+2d9 <= 12"}"#;
        let parsed_json: DiceRollRequest = serde_json::from_str(json).unwrap();

        let _ = DiceExpressionParser::parse_rpn(&parsed_json.rollstring).unwrap();
    }

    #[test]
    fn big_test() {
        let json = r#"{"iterations": 100, "rollstring": "(20d50+300d8*(3d5>3)+100+40d20kh4+100+(50d30)d20)>=3"}"#;
        let parsed_json: DiceRollRequest = serde_json::from_str(json).unwrap();
        let expr = DiceExpressionParser::parse_rpn(&parsed_json.rollstring).unwrap();
        let expr_arr = vec![Arc::new(expr); parsed_json.iterations];
        let answer: Result<Vec<i64>, EvaluationError> =
            expr_arr.par_iter().map(|iter| iter.eval()).collect();
        assert!(answer.is_ok())
    }

    #[test]
    fn massive_test() {
        let json = r#"{"iterations": 1000, "rollstring": "1d20d40d60d80d160d320d640d1280d2560d5120d10240d20480"}"#;
        let parsed_json: DiceRollRequest = serde_json::from_str(json).unwrap();
        let expr = DiceExpressionParser::parse_rpn(&parsed_json.rollstring).unwrap();
        let expr_arr = vec![Arc::new(expr); parsed_json.iterations];
        let answer: Result<Vec<i64>, EvaluationError> =
            expr_arr.par_iter().map(|iter| iter.eval()).collect();
        assert!(answer.is_err_and(|e| matches!(e, EvaluationError::IntegerOverflow)))
    }

    #[test]
    fn d0_test() {
        let json = r#"{"iterations": 1000, "rollstring": "2d0"}"#;
        let parsed_json: DiceRollRequest = serde_json::from_str(json).unwrap();
        let expr = DiceExpressionParser::parse_rpn(&parsed_json.rollstring).unwrap();
        let expr_arr = vec![Arc::new(expr); parsed_json.iterations];
        let answer: Result<Vec<i64>, EvaluationError> =
            expr_arr.par_iter().map(|iter| iter.eval()).collect();
        
        assert!(answer.is_ok_and(|v| v == vec![0; 1000]))
    }


}
