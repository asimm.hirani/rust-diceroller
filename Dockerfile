FROM rust:1.73 as builder
RUN rustup target add x86_64-unknown-linux-musl
RUN apt update && apt-get install -y musl-tools musl-dev
RUN update-ca-certificates

WORKDIR /app

COPY ./ .

RUN cargo build --target x86_64-unknown-linux-musl --release

FROM alpine:latest

WORKDIR /app

# Copy our build
COPY --from=builder /app/target/x86_64-unknown-linux-musl/release/diceroller-rs ./
COPY ./static ./static


CMD ["./diceroller-rs"]
